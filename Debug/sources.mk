################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 
OBJCOPY_BIN := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Components/cs43l22 \
Components/exc7200 \
Components/ft6x06 \
Components/i3g4250d \
Components/ili9325 \
Components/ili9341 \
Components/l3gd20 \
Components/lis302dl \
Components/lis3dsh \
Components/ls016b8uy \
Components/lsm303agr \
Components/lsm303dlhc \
Components/mfxstm32l152 \
Components/nt35510 \
Components/otm8009a \
Components/ov2640 \
Components/ov5640 \
Components/s5k5cag \
Components/st7735 \
Components/st7789h2 \
Components/stmpe1600 \
Components/stmpe811 \
Components/ts3510 \
Components/wm8994 \
Core/Src \
Core/Startup \
Drivers/STM32F4xx_HAL_Driver/Src \
Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Src \
Middlewares/ST/STM32_USB_Host_Library/Core/Src \
STM32F411E-Discovery \
USB_HOST/App \
USB_HOST/Target \

