################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/TransformFunctions/arm_bitreversal.c \
../Source/TransformFunctions/arm_cfft_f32.c \
../Source/TransformFunctions/arm_cfft_q15.c \
../Source/TransformFunctions/arm_cfft_q31.c \
../Source/TransformFunctions/arm_cfft_radix2_f32.c \
../Source/TransformFunctions/arm_cfft_radix2_init_f32.c \
../Source/TransformFunctions/arm_cfft_radix2_init_q15.c \
../Source/TransformFunctions/arm_cfft_radix2_init_q31.c \
../Source/TransformFunctions/arm_cfft_radix2_q15.c \
../Source/TransformFunctions/arm_cfft_radix2_q31.c \
../Source/TransformFunctions/arm_cfft_radix4_f32.c \
../Source/TransformFunctions/arm_cfft_radix4_init_f32.c \
../Source/TransformFunctions/arm_cfft_radix4_init_q15.c \
../Source/TransformFunctions/arm_cfft_radix4_init_q31.c \
../Source/TransformFunctions/arm_cfft_radix4_q15.c \
../Source/TransformFunctions/arm_cfft_radix4_q31.c \
../Source/TransformFunctions/arm_cfft_radix8_f32.c \
../Source/TransformFunctions/arm_dct4_f32.c \
../Source/TransformFunctions/arm_dct4_init_f32.c \
../Source/TransformFunctions/arm_dct4_init_q15.c \
../Source/TransformFunctions/arm_dct4_init_q31.c \
../Source/TransformFunctions/arm_dct4_q15.c \
../Source/TransformFunctions/arm_dct4_q31.c \
../Source/TransformFunctions/arm_rfft_f32.c \
../Source/TransformFunctions/arm_rfft_fast_f32.c \
../Source/TransformFunctions/arm_rfft_fast_init_f32.c \
../Source/TransformFunctions/arm_rfft_init_f32.c \
../Source/TransformFunctions/arm_rfft_init_q15.c \
../Source/TransformFunctions/arm_rfft_init_q31.c \
../Source/TransformFunctions/arm_rfft_q15.c \
../Source/TransformFunctions/arm_rfft_q31.c 

S_UPPER_SRCS += \
../Source/TransformFunctions/arm_bitreversal2.S 

OBJS += \
./Source/TransformFunctions/arm_bitreversal.o \
./Source/TransformFunctions/arm_bitreversal2.o \
./Source/TransformFunctions/arm_cfft_f32.o \
./Source/TransformFunctions/arm_cfft_q15.o \
./Source/TransformFunctions/arm_cfft_q31.o \
./Source/TransformFunctions/arm_cfft_radix2_f32.o \
./Source/TransformFunctions/arm_cfft_radix2_init_f32.o \
./Source/TransformFunctions/arm_cfft_radix2_init_q15.o \
./Source/TransformFunctions/arm_cfft_radix2_init_q31.o \
./Source/TransformFunctions/arm_cfft_radix2_q15.o \
./Source/TransformFunctions/arm_cfft_radix2_q31.o \
./Source/TransformFunctions/arm_cfft_radix4_f32.o \
./Source/TransformFunctions/arm_cfft_radix4_init_f32.o \
./Source/TransformFunctions/arm_cfft_radix4_init_q15.o \
./Source/TransformFunctions/arm_cfft_radix4_init_q31.o \
./Source/TransformFunctions/arm_cfft_radix4_q15.o \
./Source/TransformFunctions/arm_cfft_radix4_q31.o \
./Source/TransformFunctions/arm_cfft_radix8_f32.o \
./Source/TransformFunctions/arm_dct4_f32.o \
./Source/TransformFunctions/arm_dct4_init_f32.o \
./Source/TransformFunctions/arm_dct4_init_q15.o \
./Source/TransformFunctions/arm_dct4_init_q31.o \
./Source/TransformFunctions/arm_dct4_q15.o \
./Source/TransformFunctions/arm_dct4_q31.o \
./Source/TransformFunctions/arm_rfft_f32.o \
./Source/TransformFunctions/arm_rfft_fast_f32.o \
./Source/TransformFunctions/arm_rfft_fast_init_f32.o \
./Source/TransformFunctions/arm_rfft_init_f32.o \
./Source/TransformFunctions/arm_rfft_init_q15.o \
./Source/TransformFunctions/arm_rfft_init_q31.o \
./Source/TransformFunctions/arm_rfft_q15.o \
./Source/TransformFunctions/arm_rfft_q31.o 

S_UPPER_DEPS += \
./Source/TransformFunctions/arm_bitreversal2.d 

C_DEPS += \
./Source/TransformFunctions/arm_bitreversal.d \
./Source/TransformFunctions/arm_cfft_f32.d \
./Source/TransformFunctions/arm_cfft_q15.d \
./Source/TransformFunctions/arm_cfft_q31.d \
./Source/TransformFunctions/arm_cfft_radix2_f32.d \
./Source/TransformFunctions/arm_cfft_radix2_init_f32.d \
./Source/TransformFunctions/arm_cfft_radix2_init_q15.d \
./Source/TransformFunctions/arm_cfft_radix2_init_q31.d \
./Source/TransformFunctions/arm_cfft_radix2_q15.d \
./Source/TransformFunctions/arm_cfft_radix2_q31.d \
./Source/TransformFunctions/arm_cfft_radix4_f32.d \
./Source/TransformFunctions/arm_cfft_radix4_init_f32.d \
./Source/TransformFunctions/arm_cfft_radix4_init_q15.d \
./Source/TransformFunctions/arm_cfft_radix4_init_q31.d \
./Source/TransformFunctions/arm_cfft_radix4_q15.d \
./Source/TransformFunctions/arm_cfft_radix4_q31.d \
./Source/TransformFunctions/arm_cfft_radix8_f32.d \
./Source/TransformFunctions/arm_dct4_f32.d \
./Source/TransformFunctions/arm_dct4_init_f32.d \
./Source/TransformFunctions/arm_dct4_init_q15.d \
./Source/TransformFunctions/arm_dct4_init_q31.d \
./Source/TransformFunctions/arm_dct4_q15.d \
./Source/TransformFunctions/arm_dct4_q31.d \
./Source/TransformFunctions/arm_rfft_f32.d \
./Source/TransformFunctions/arm_rfft_fast_f32.d \
./Source/TransformFunctions/arm_rfft_fast_init_f32.d \
./Source/TransformFunctions/arm_rfft_init_f32.d \
./Source/TransformFunctions/arm_rfft_init_q15.d \
./Source/TransformFunctions/arm_rfft_init_q31.d \
./Source/TransformFunctions/arm_rfft_q15.d \
./Source/TransformFunctions/arm_rfft_q31.d 


# Each subdirectory must supply rules for building sources it contributes
Source/TransformFunctions/%.o: ../Source/TransformFunctions/%.c Source/TransformFunctions/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Source/TransformFunctions/%.o: ../Source/TransformFunctions/%.S Source/TransformFunctions/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m4 -g3 -DDEBUG -c -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@" "$<"

