################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/SupportFunctions/arm_copy_f32.c \
../Source/SupportFunctions/arm_copy_q15.c \
../Source/SupportFunctions/arm_copy_q31.c \
../Source/SupportFunctions/arm_copy_q7.c \
../Source/SupportFunctions/arm_fill_f32.c \
../Source/SupportFunctions/arm_fill_q15.c \
../Source/SupportFunctions/arm_fill_q31.c \
../Source/SupportFunctions/arm_fill_q7.c \
../Source/SupportFunctions/arm_float_to_q15.c \
../Source/SupportFunctions/arm_float_to_q31.c \
../Source/SupportFunctions/arm_float_to_q7.c \
../Source/SupportFunctions/arm_q15_to_float.c \
../Source/SupportFunctions/arm_q15_to_q31.c \
../Source/SupportFunctions/arm_q15_to_q7.c \
../Source/SupportFunctions/arm_q31_to_float.c \
../Source/SupportFunctions/arm_q31_to_q15.c \
../Source/SupportFunctions/arm_q31_to_q7.c \
../Source/SupportFunctions/arm_q7_to_float.c \
../Source/SupportFunctions/arm_q7_to_q15.c \
../Source/SupportFunctions/arm_q7_to_q31.c 

OBJS += \
./Source/SupportFunctions/arm_copy_f32.o \
./Source/SupportFunctions/arm_copy_q15.o \
./Source/SupportFunctions/arm_copy_q31.o \
./Source/SupportFunctions/arm_copy_q7.o \
./Source/SupportFunctions/arm_fill_f32.o \
./Source/SupportFunctions/arm_fill_q15.o \
./Source/SupportFunctions/arm_fill_q31.o \
./Source/SupportFunctions/arm_fill_q7.o \
./Source/SupportFunctions/arm_float_to_q15.o \
./Source/SupportFunctions/arm_float_to_q31.o \
./Source/SupportFunctions/arm_float_to_q7.o \
./Source/SupportFunctions/arm_q15_to_float.o \
./Source/SupportFunctions/arm_q15_to_q31.o \
./Source/SupportFunctions/arm_q15_to_q7.o \
./Source/SupportFunctions/arm_q31_to_float.o \
./Source/SupportFunctions/arm_q31_to_q15.o \
./Source/SupportFunctions/arm_q31_to_q7.o \
./Source/SupportFunctions/arm_q7_to_float.o \
./Source/SupportFunctions/arm_q7_to_q15.o \
./Source/SupportFunctions/arm_q7_to_q31.o 

C_DEPS += \
./Source/SupportFunctions/arm_copy_f32.d \
./Source/SupportFunctions/arm_copy_q15.d \
./Source/SupportFunctions/arm_copy_q31.d \
./Source/SupportFunctions/arm_copy_q7.d \
./Source/SupportFunctions/arm_fill_f32.d \
./Source/SupportFunctions/arm_fill_q15.d \
./Source/SupportFunctions/arm_fill_q31.d \
./Source/SupportFunctions/arm_fill_q7.d \
./Source/SupportFunctions/arm_float_to_q15.d \
./Source/SupportFunctions/arm_float_to_q31.d \
./Source/SupportFunctions/arm_float_to_q7.d \
./Source/SupportFunctions/arm_q15_to_float.d \
./Source/SupportFunctions/arm_q15_to_q31.d \
./Source/SupportFunctions/arm_q15_to_q7.d \
./Source/SupportFunctions/arm_q31_to_float.d \
./Source/SupportFunctions/arm_q31_to_q15.d \
./Source/SupportFunctions/arm_q31_to_q7.d \
./Source/SupportFunctions/arm_q7_to_float.d \
./Source/SupportFunctions/arm_q7_to_q15.d \
./Source/SupportFunctions/arm_q7_to_q31.d 


# Each subdirectory must supply rules for building sources it contributes
Source/SupportFunctions/%.o: ../Source/SupportFunctions/%.c Source/SupportFunctions/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

