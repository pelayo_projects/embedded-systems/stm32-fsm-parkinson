################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/StatisticsFunctions/arm_max_f32.c \
../Source/StatisticsFunctions/arm_max_q15.c \
../Source/StatisticsFunctions/arm_max_q31.c \
../Source/StatisticsFunctions/arm_max_q7.c \
../Source/StatisticsFunctions/arm_mean_f32.c \
../Source/StatisticsFunctions/arm_mean_q15.c \
../Source/StatisticsFunctions/arm_mean_q31.c \
../Source/StatisticsFunctions/arm_mean_q7.c \
../Source/StatisticsFunctions/arm_min_f32.c \
../Source/StatisticsFunctions/arm_min_q15.c \
../Source/StatisticsFunctions/arm_min_q31.c \
../Source/StatisticsFunctions/arm_min_q7.c \
../Source/StatisticsFunctions/arm_power_f32.c \
../Source/StatisticsFunctions/arm_power_q15.c \
../Source/StatisticsFunctions/arm_power_q31.c \
../Source/StatisticsFunctions/arm_power_q7.c \
../Source/StatisticsFunctions/arm_rms_f32.c \
../Source/StatisticsFunctions/arm_rms_q15.c \
../Source/StatisticsFunctions/arm_rms_q31.c \
../Source/StatisticsFunctions/arm_std_f32.c \
../Source/StatisticsFunctions/arm_std_q15.c \
../Source/StatisticsFunctions/arm_std_q31.c \
../Source/StatisticsFunctions/arm_var_f32.c \
../Source/StatisticsFunctions/arm_var_q15.c \
../Source/StatisticsFunctions/arm_var_q31.c 

OBJS += \
./Source/StatisticsFunctions/arm_max_f32.o \
./Source/StatisticsFunctions/arm_max_q15.o \
./Source/StatisticsFunctions/arm_max_q31.o \
./Source/StatisticsFunctions/arm_max_q7.o \
./Source/StatisticsFunctions/arm_mean_f32.o \
./Source/StatisticsFunctions/arm_mean_q15.o \
./Source/StatisticsFunctions/arm_mean_q31.o \
./Source/StatisticsFunctions/arm_mean_q7.o \
./Source/StatisticsFunctions/arm_min_f32.o \
./Source/StatisticsFunctions/arm_min_q15.o \
./Source/StatisticsFunctions/arm_min_q31.o \
./Source/StatisticsFunctions/arm_min_q7.o \
./Source/StatisticsFunctions/arm_power_f32.o \
./Source/StatisticsFunctions/arm_power_q15.o \
./Source/StatisticsFunctions/arm_power_q31.o \
./Source/StatisticsFunctions/arm_power_q7.o \
./Source/StatisticsFunctions/arm_rms_f32.o \
./Source/StatisticsFunctions/arm_rms_q15.o \
./Source/StatisticsFunctions/arm_rms_q31.o \
./Source/StatisticsFunctions/arm_std_f32.o \
./Source/StatisticsFunctions/arm_std_q15.o \
./Source/StatisticsFunctions/arm_std_q31.o \
./Source/StatisticsFunctions/arm_var_f32.o \
./Source/StatisticsFunctions/arm_var_q15.o \
./Source/StatisticsFunctions/arm_var_q31.o 

C_DEPS += \
./Source/StatisticsFunctions/arm_max_f32.d \
./Source/StatisticsFunctions/arm_max_q15.d \
./Source/StatisticsFunctions/arm_max_q31.d \
./Source/StatisticsFunctions/arm_max_q7.d \
./Source/StatisticsFunctions/arm_mean_f32.d \
./Source/StatisticsFunctions/arm_mean_q15.d \
./Source/StatisticsFunctions/arm_mean_q31.d \
./Source/StatisticsFunctions/arm_mean_q7.d \
./Source/StatisticsFunctions/arm_min_f32.d \
./Source/StatisticsFunctions/arm_min_q15.d \
./Source/StatisticsFunctions/arm_min_q31.d \
./Source/StatisticsFunctions/arm_min_q7.d \
./Source/StatisticsFunctions/arm_power_f32.d \
./Source/StatisticsFunctions/arm_power_q15.d \
./Source/StatisticsFunctions/arm_power_q31.d \
./Source/StatisticsFunctions/arm_power_q7.d \
./Source/StatisticsFunctions/arm_rms_f32.d \
./Source/StatisticsFunctions/arm_rms_q15.d \
./Source/StatisticsFunctions/arm_rms_q31.d \
./Source/StatisticsFunctions/arm_std_f32.d \
./Source/StatisticsFunctions/arm_std_q15.d \
./Source/StatisticsFunctions/arm_std_q31.d \
./Source/StatisticsFunctions/arm_var_f32.d \
./Source/StatisticsFunctions/arm_var_q15.d \
./Source/StatisticsFunctions/arm_var_q31.d 


# Each subdirectory must supply rules for building sources it contributes
Source/StatisticsFunctions/%.o: ../Source/StatisticsFunctions/%.c Source/StatisticsFunctions/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

