################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/MatrixFunctions/arm_mat_add_f32.c \
../Source/MatrixFunctions/arm_mat_add_q15.c \
../Source/MatrixFunctions/arm_mat_add_q31.c \
../Source/MatrixFunctions/arm_mat_cmplx_mult_f32.c \
../Source/MatrixFunctions/arm_mat_cmplx_mult_q15.c \
../Source/MatrixFunctions/arm_mat_cmplx_mult_q31.c \
../Source/MatrixFunctions/arm_mat_init_f32.c \
../Source/MatrixFunctions/arm_mat_init_q15.c \
../Source/MatrixFunctions/arm_mat_init_q31.c \
../Source/MatrixFunctions/arm_mat_inverse_f32.c \
../Source/MatrixFunctions/arm_mat_inverse_f64.c \
../Source/MatrixFunctions/arm_mat_mult_f32.c \
../Source/MatrixFunctions/arm_mat_mult_fast_q15.c \
../Source/MatrixFunctions/arm_mat_mult_fast_q31.c \
../Source/MatrixFunctions/arm_mat_mult_q15.c \
../Source/MatrixFunctions/arm_mat_mult_q31.c \
../Source/MatrixFunctions/arm_mat_scale_f32.c \
../Source/MatrixFunctions/arm_mat_scale_q15.c \
../Source/MatrixFunctions/arm_mat_scale_q31.c \
../Source/MatrixFunctions/arm_mat_sub_f32.c \
../Source/MatrixFunctions/arm_mat_sub_q15.c \
../Source/MatrixFunctions/arm_mat_sub_q31.c \
../Source/MatrixFunctions/arm_mat_trans_f32.c \
../Source/MatrixFunctions/arm_mat_trans_q15.c \
../Source/MatrixFunctions/arm_mat_trans_q31.c 

OBJS += \
./Source/MatrixFunctions/arm_mat_add_f32.o \
./Source/MatrixFunctions/arm_mat_add_q15.o \
./Source/MatrixFunctions/arm_mat_add_q31.o \
./Source/MatrixFunctions/arm_mat_cmplx_mult_f32.o \
./Source/MatrixFunctions/arm_mat_cmplx_mult_q15.o \
./Source/MatrixFunctions/arm_mat_cmplx_mult_q31.o \
./Source/MatrixFunctions/arm_mat_init_f32.o \
./Source/MatrixFunctions/arm_mat_init_q15.o \
./Source/MatrixFunctions/arm_mat_init_q31.o \
./Source/MatrixFunctions/arm_mat_inverse_f32.o \
./Source/MatrixFunctions/arm_mat_inverse_f64.o \
./Source/MatrixFunctions/arm_mat_mult_f32.o \
./Source/MatrixFunctions/arm_mat_mult_fast_q15.o \
./Source/MatrixFunctions/arm_mat_mult_fast_q31.o \
./Source/MatrixFunctions/arm_mat_mult_q15.o \
./Source/MatrixFunctions/arm_mat_mult_q31.o \
./Source/MatrixFunctions/arm_mat_scale_f32.o \
./Source/MatrixFunctions/arm_mat_scale_q15.o \
./Source/MatrixFunctions/arm_mat_scale_q31.o \
./Source/MatrixFunctions/arm_mat_sub_f32.o \
./Source/MatrixFunctions/arm_mat_sub_q15.o \
./Source/MatrixFunctions/arm_mat_sub_q31.o \
./Source/MatrixFunctions/arm_mat_trans_f32.o \
./Source/MatrixFunctions/arm_mat_trans_q15.o \
./Source/MatrixFunctions/arm_mat_trans_q31.o 

C_DEPS += \
./Source/MatrixFunctions/arm_mat_add_f32.d \
./Source/MatrixFunctions/arm_mat_add_q15.d \
./Source/MatrixFunctions/arm_mat_add_q31.d \
./Source/MatrixFunctions/arm_mat_cmplx_mult_f32.d \
./Source/MatrixFunctions/arm_mat_cmplx_mult_q15.d \
./Source/MatrixFunctions/arm_mat_cmplx_mult_q31.d \
./Source/MatrixFunctions/arm_mat_init_f32.d \
./Source/MatrixFunctions/arm_mat_init_q15.d \
./Source/MatrixFunctions/arm_mat_init_q31.d \
./Source/MatrixFunctions/arm_mat_inverse_f32.d \
./Source/MatrixFunctions/arm_mat_inverse_f64.d \
./Source/MatrixFunctions/arm_mat_mult_f32.d \
./Source/MatrixFunctions/arm_mat_mult_fast_q15.d \
./Source/MatrixFunctions/arm_mat_mult_fast_q31.d \
./Source/MatrixFunctions/arm_mat_mult_q15.d \
./Source/MatrixFunctions/arm_mat_mult_q31.d \
./Source/MatrixFunctions/arm_mat_scale_f32.d \
./Source/MatrixFunctions/arm_mat_scale_q15.d \
./Source/MatrixFunctions/arm_mat_scale_q31.d \
./Source/MatrixFunctions/arm_mat_sub_f32.d \
./Source/MatrixFunctions/arm_mat_sub_q15.d \
./Source/MatrixFunctions/arm_mat_sub_q31.d \
./Source/MatrixFunctions/arm_mat_trans_f32.d \
./Source/MatrixFunctions/arm_mat_trans_q15.d \
./Source/MatrixFunctions/arm_mat_trans_q31.d 


# Each subdirectory must supply rules for building sources it contributes
Source/MatrixFunctions/%.o: ../Source/MatrixFunctions/%.c Source/MatrixFunctions/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

