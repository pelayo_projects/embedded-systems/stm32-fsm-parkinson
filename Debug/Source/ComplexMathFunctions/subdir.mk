################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/ComplexMathFunctions/arm_cmplx_conj_f32.c \
../Source/ComplexMathFunctions/arm_cmplx_conj_q15.c \
../Source/ComplexMathFunctions/arm_cmplx_conj_q31.c \
../Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.c \
../Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.c \
../Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.c \
../Source/ComplexMathFunctions/arm_cmplx_mag_f32.c \
../Source/ComplexMathFunctions/arm_cmplx_mag_q15.c \
../Source/ComplexMathFunctions/arm_cmplx_mag_q31.c \
../Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.c \
../Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.c \
../Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.c \
../Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.c \
../Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.c \
../Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.c \
../Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.c \
../Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.c \
../Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.c 

OBJS += \
./Source/ComplexMathFunctions/arm_cmplx_conj_f32.o \
./Source/ComplexMathFunctions/arm_cmplx_conj_q15.o \
./Source/ComplexMathFunctions/arm_cmplx_conj_q31.o \
./Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.o \
./Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.o \
./Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.o \
./Source/ComplexMathFunctions/arm_cmplx_mag_f32.o \
./Source/ComplexMathFunctions/arm_cmplx_mag_q15.o \
./Source/ComplexMathFunctions/arm_cmplx_mag_q31.o \
./Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.o \
./Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.o \
./Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.o \
./Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.o \
./Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.o \
./Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.o \
./Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.o \
./Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.o \
./Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.o 

C_DEPS += \
./Source/ComplexMathFunctions/arm_cmplx_conj_f32.d \
./Source/ComplexMathFunctions/arm_cmplx_conj_q15.d \
./Source/ComplexMathFunctions/arm_cmplx_conj_q31.d \
./Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.d \
./Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.d \
./Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.d \
./Source/ComplexMathFunctions/arm_cmplx_mag_f32.d \
./Source/ComplexMathFunctions/arm_cmplx_mag_q15.d \
./Source/ComplexMathFunctions/arm_cmplx_mag_q31.d \
./Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.d \
./Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.d \
./Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.d \
./Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.d \
./Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.d \
./Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.d \
./Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.d \
./Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.d \
./Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.d 


# Each subdirectory must supply rules for building sources it contributes
Source/ComplexMathFunctions/%.o: ../Source/ComplexMathFunctions/%.c Source/ComplexMathFunctions/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

