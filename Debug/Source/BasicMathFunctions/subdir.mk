################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/BasicMathFunctions/arm_abs_f32.c \
../Source/BasicMathFunctions/arm_abs_q15.c \
../Source/BasicMathFunctions/arm_abs_q31.c \
../Source/BasicMathFunctions/arm_abs_q7.c \
../Source/BasicMathFunctions/arm_add_f32.c \
../Source/BasicMathFunctions/arm_add_q15.c \
../Source/BasicMathFunctions/arm_add_q31.c \
../Source/BasicMathFunctions/arm_add_q7.c \
../Source/BasicMathFunctions/arm_dot_prod_f32.c \
../Source/BasicMathFunctions/arm_dot_prod_q15.c \
../Source/BasicMathFunctions/arm_dot_prod_q31.c \
../Source/BasicMathFunctions/arm_dot_prod_q7.c \
../Source/BasicMathFunctions/arm_mult_f32.c \
../Source/BasicMathFunctions/arm_mult_q15.c \
../Source/BasicMathFunctions/arm_mult_q31.c \
../Source/BasicMathFunctions/arm_mult_q7.c \
../Source/BasicMathFunctions/arm_negate_f32.c \
../Source/BasicMathFunctions/arm_negate_q15.c \
../Source/BasicMathFunctions/arm_negate_q31.c \
../Source/BasicMathFunctions/arm_negate_q7.c \
../Source/BasicMathFunctions/arm_offset_f32.c \
../Source/BasicMathFunctions/arm_offset_q15.c \
../Source/BasicMathFunctions/arm_offset_q31.c \
../Source/BasicMathFunctions/arm_offset_q7.c \
../Source/BasicMathFunctions/arm_scale_f32.c \
../Source/BasicMathFunctions/arm_scale_q15.c \
../Source/BasicMathFunctions/arm_scale_q31.c \
../Source/BasicMathFunctions/arm_scale_q7.c \
../Source/BasicMathFunctions/arm_shift_q15.c \
../Source/BasicMathFunctions/arm_shift_q31.c \
../Source/BasicMathFunctions/arm_shift_q7.c \
../Source/BasicMathFunctions/arm_sub_f32.c \
../Source/BasicMathFunctions/arm_sub_q15.c \
../Source/BasicMathFunctions/arm_sub_q31.c \
../Source/BasicMathFunctions/arm_sub_q7.c 

OBJS += \
./Source/BasicMathFunctions/arm_abs_f32.o \
./Source/BasicMathFunctions/arm_abs_q15.o \
./Source/BasicMathFunctions/arm_abs_q31.o \
./Source/BasicMathFunctions/arm_abs_q7.o \
./Source/BasicMathFunctions/arm_add_f32.o \
./Source/BasicMathFunctions/arm_add_q15.o \
./Source/BasicMathFunctions/arm_add_q31.o \
./Source/BasicMathFunctions/arm_add_q7.o \
./Source/BasicMathFunctions/arm_dot_prod_f32.o \
./Source/BasicMathFunctions/arm_dot_prod_q15.o \
./Source/BasicMathFunctions/arm_dot_prod_q31.o \
./Source/BasicMathFunctions/arm_dot_prod_q7.o \
./Source/BasicMathFunctions/arm_mult_f32.o \
./Source/BasicMathFunctions/arm_mult_q15.o \
./Source/BasicMathFunctions/arm_mult_q31.o \
./Source/BasicMathFunctions/arm_mult_q7.o \
./Source/BasicMathFunctions/arm_negate_f32.o \
./Source/BasicMathFunctions/arm_negate_q15.o \
./Source/BasicMathFunctions/arm_negate_q31.o \
./Source/BasicMathFunctions/arm_negate_q7.o \
./Source/BasicMathFunctions/arm_offset_f32.o \
./Source/BasicMathFunctions/arm_offset_q15.o \
./Source/BasicMathFunctions/arm_offset_q31.o \
./Source/BasicMathFunctions/arm_offset_q7.o \
./Source/BasicMathFunctions/arm_scale_f32.o \
./Source/BasicMathFunctions/arm_scale_q15.o \
./Source/BasicMathFunctions/arm_scale_q31.o \
./Source/BasicMathFunctions/arm_scale_q7.o \
./Source/BasicMathFunctions/arm_shift_q15.o \
./Source/BasicMathFunctions/arm_shift_q31.o \
./Source/BasicMathFunctions/arm_shift_q7.o \
./Source/BasicMathFunctions/arm_sub_f32.o \
./Source/BasicMathFunctions/arm_sub_q15.o \
./Source/BasicMathFunctions/arm_sub_q31.o \
./Source/BasicMathFunctions/arm_sub_q7.o 

C_DEPS += \
./Source/BasicMathFunctions/arm_abs_f32.d \
./Source/BasicMathFunctions/arm_abs_q15.d \
./Source/BasicMathFunctions/arm_abs_q31.d \
./Source/BasicMathFunctions/arm_abs_q7.d \
./Source/BasicMathFunctions/arm_add_f32.d \
./Source/BasicMathFunctions/arm_add_q15.d \
./Source/BasicMathFunctions/arm_add_q31.d \
./Source/BasicMathFunctions/arm_add_q7.d \
./Source/BasicMathFunctions/arm_dot_prod_f32.d \
./Source/BasicMathFunctions/arm_dot_prod_q15.d \
./Source/BasicMathFunctions/arm_dot_prod_q31.d \
./Source/BasicMathFunctions/arm_dot_prod_q7.d \
./Source/BasicMathFunctions/arm_mult_f32.d \
./Source/BasicMathFunctions/arm_mult_q15.d \
./Source/BasicMathFunctions/arm_mult_q31.d \
./Source/BasicMathFunctions/arm_mult_q7.d \
./Source/BasicMathFunctions/arm_negate_f32.d \
./Source/BasicMathFunctions/arm_negate_q15.d \
./Source/BasicMathFunctions/arm_negate_q31.d \
./Source/BasicMathFunctions/arm_negate_q7.d \
./Source/BasicMathFunctions/arm_offset_f32.d \
./Source/BasicMathFunctions/arm_offset_q15.d \
./Source/BasicMathFunctions/arm_offset_q31.d \
./Source/BasicMathFunctions/arm_offset_q7.d \
./Source/BasicMathFunctions/arm_scale_f32.d \
./Source/BasicMathFunctions/arm_scale_q15.d \
./Source/BasicMathFunctions/arm_scale_q31.d \
./Source/BasicMathFunctions/arm_scale_q7.d \
./Source/BasicMathFunctions/arm_shift_q15.d \
./Source/BasicMathFunctions/arm_shift_q31.d \
./Source/BasicMathFunctions/arm_shift_q7.d \
./Source/BasicMathFunctions/arm_sub_f32.d \
./Source/BasicMathFunctions/arm_sub_q15.d \
./Source/BasicMathFunctions/arm_sub_q31.d \
./Source/BasicMathFunctions/arm_sub_q7.d 


# Each subdirectory must supply rules for building sources it contributes
Source/BasicMathFunctions/%.o: ../Source/BasicMathFunctions/%.c Source/BasicMathFunctions/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"/home/badr/Escritorio/workspace/SE/Practica2/STM32F411E-Discovery" -I"/home/badr/Escritorio/workspace/SE/Practica2/Components" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

